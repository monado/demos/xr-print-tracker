{%- extends 'base.cpp' -%}
{%- macro fn() -%}{{stem}}.h{%- endmacro -%}
{%- block brief %}Header{% endblock -%}
{% block content %}
#pragma once

// Internal Includes
// - none

// Library Includes
// - none

// Standard Includes
// - none

{% endblock -%}
