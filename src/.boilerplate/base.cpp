{%- from 'base' import notice -%}
{%- block fileheader -%}
{{- notice | cpp_line_comment('// ') }}
/*!
 * @file
 * @brief  {% block brief %}{% endblock %}
 * @author {{author}}
 */
{% endblock -%}
{% block content %}{% endblock %}
