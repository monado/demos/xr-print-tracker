// Copyright 2019-2022, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Implementation
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

// Internal Includes
#include "OpenXRPlatformIncludeWrapper.h"

// Library Includes
#include "fmt/core.h"
#include "openxr/openxr_atoms.hpp"
#include "openxr/openxr_dispatch_dynamic.hpp"
#include "openxr/openxr_handles.hpp"
#include "openxr/openxr_method_impls.hpp"
#include "openxr/openxr_structs.hpp"

// Standard Includes
#include <algorithm>
#include <iomanip>
#include <iostream>
#include <string>
#include <thread>
#include <vector>

// #define FORCE_API_DUMP
// #define FORCE_VALIDATION

#include <fmt/format.h>

static const uint32_t numViews_ = 2;
#define NUMBER_FORMAT std::setw(6) << std::setprecision(3)
static inline void print(XrQuaternionf const &quat) {
    fmt::print("(({:+04.3f}, {:+04.3f}, {:+04.3f}), {:+04.3f})", quat.x, quat.y, quat.z, quat.w);
}

static inline void print(XrVector3f const &vec) { fmt::print("({:+04.3f}, {:+04.3f}, {:+04.3f})", vec.x, vec.y, vec.z); }

static inline void print(XrPosef const &pose) {
    std::cout << "Position: ";
    print(pose.position);
    std ::cout << "  Orientation: ";
    print(pose.orientation);
}

static bool hasApiLayer(std::vector<xr::ApiLayerProperties> const &apiLayers, const char *name) {
    auto b = apiLayers.begin();
    auto e = apiLayers.end();

    const std::string name_string = name;

    return (e != std::find_if(b, e, [&](const xr::ApiLayerProperties &prop) { return prop.layerName == name_string; }));
}

static void conditionallyAddApiLayer(std::vector<xr::ApiLayerProperties> const &apiLayers, const char *name,
                                     std::vector<const char *> &out_enabled) {
    if (hasApiLayer(apiLayers, name)) {
        // OK we have this layer.
        out_enabled.push_back(name);
    }
}

static bool hasExtension(std::vector<xr::ExtensionProperties> const &extensions, const char *name) {
    auto b = extensions.begin();
    auto e = extensions.end();

    const std::string name_string = name;

    return (e !=
            std::find_if(b, e, [&](const xr::ExtensionProperties &prop) { return prop.extensionName == name_string; }));
}

template <typename Dispatch> xr::Time getXrTimeNow(xr::Instance instance, Dispatch &&d) {
#ifdef _WIN32
    LARGE_INTEGER qpc;
    QueryPerformanceCounter(&qpc);
    return instance.convertWin32PerformanceCounterToTimeKHR(&qpc, d);

#else
    struct timespec t;
    clock_gettime(CLOCK_REALTIME, &t);
    return instance.convertTimespecTimeToTimeKHR(&t, d);
#endif
}

template <typename Dispatch> void pollEvent(xr::Instance instance, Dispatch &&d) {
    while (1) {
        xr::EventDataBuffer event;
        auto result = instance.pollEvent(event, d);
        if (result == xr::Result::EventUnavailable) {
            return;
        } else if (result != xr::Result::Success) {
            std::cout << "Got error polling for events: " << to_string(result) << std::endl;
            return;
        }

        std::cout << "Event: " << to_string_literal(event.type) << std::endl;
        if (event.type == xr::StructureType::EventDataSessionStateChanged) {
            auto &change = reinterpret_cast<xr::EventDataSessionStateChanged &>(event);
            std::cout << to_string(change.state) << std::endl;
        }
    }
}
enum class Backend {
    None,
    Headless,
    OpenGL,
    Vulkan,
    D3D11,
};

constexpr bool allowHeadless = true;

int main(int /* argc */, char * /* argv */[]) {

    xr::UniqueDynamicInstance instance;
    auto d = xr::DispatchLoaderDynamic{};

    Backend backend = Backend::None;

    // Set up instance.
    {
        // Using the openxr.hpp wrapper that performs the whole two-call operation in one step
        auto apiLayers = xr::enumerateApiLayerPropertiesToVector(d);
        std::cout << "Enumerated layers:\n";
        for (const auto &prop : apiLayers) {
            std::cout << prop.layerName << " - " << prop.description << std::endl;
        }
        std::cout << "Number of layers: " << apiLayers.size() << std::endl;

        std::vector<const char *> enabledLayers;

        // Using the openxr.hpp wrapper that performs the whole two-call operation in one step
        auto extensions = xr::enumerateInstanceExtensionPropertiesToVector(nullptr, d);

        std::vector<const char *> enabledExtensions;
        if (allowHeadless && hasExtension(extensions, XR_MND_HEADLESS_EXTENSION_NAME) &&
            hasExtension(extensions, XR_KHR_CONVERT_TIMESPEC_TIME_EXTENSION_NAME)) {
            std::cout << "Using headless" << std::endl;
            backend = Backend::Headless;

            enabledExtensions.push_back(XR_MND_HEADLESS_EXTENSION_NAME);
#ifdef _WIN32
            enabledExtensions.push_back(XR_KHR_WIN32_CONVERT_PERFORMANCE_COUNTER_TIME_EXTENSION_NAME);
#else
            enabledExtensions.push_back(XR_KHR_CONVERT_TIMESPEC_TIME_EXTENSION_NAME);
#endif

        }
#ifdef XR_USE_GRAPHICS_API_OPENGL
        else if (hasExtension(extensions, XR_KHR_OPENGL_ENABLE_EXTENSION_NAME)) {
            std::cout << "Using OpenGL" << std::endl;
            backend = Backend::OpenGL;
            enabledExtensions.push_back(XR_KHR_OPENGL_ENABLE_EXTENSION_NAME);
        }
#endif // XR_USE_GRAPHICS_API_VULKAN
#ifdef XR_USE_GRAPHICS_API_VULKAN
        else if (hasExtension(extensions, XR_KHR_VULKAN_ENABLE_EXTENSION_NAME)) {
            std::cout << "Using Vulkan" << std::endl;
            backend = Backend::Vulkan;
            enabledExtensions.push_back(XR_KHR_VULKAN_ENABLE_EXTENSION_NAME);
        }
#endif // XR_USE_GRAPHICS_API_VULKAN
#ifdef XR_USE_GRAPHICS_API_D3D11
        else if (hasExtension(extensions, XR_KHR_D3D11_ENABLE_EXTENSION_NAME)) {
            std::cout << "Using D3D11" << std::endl;
            backend = Backend::D3D11;
            enabledExtensions.push_back(XR_KHR_D3D11_ENABLE_EXTENSION_NAME);
        }
#endif // XR_USE_GRAPHICS_API_D3D11
        else {
            throw std::runtime_error("Could not find enough extensions to make a session!");
        }

        instance = xr::createInstanceUnique(
            xr::InstanceCreateInfo{
                xr::InstanceCreateFlagBits::None,
                xr::ApplicationInfo{"OpenXR Print Tracker", 1, // app version
                                    "", 0,                     // engine version
                                    xr::Version::current()},
                uint32_t(enabledLayers.size()),
                enabledLayers.data(),
                uint32_t(enabledExtensions.size()),
                enabledExtensions.data(),
            },
            d);
    }

    // Update the dispatch now that we have an instance
    d = xr::DispatchLoaderDynamic::createFullyPopulated(instance.get(), &::xrGetInstanceProcAddr);

    pollEvent(instance.get(), d);

    xr::UniqueDynamicSession session;
    // Get system, set up session
    {
        xr::SystemId systemId = instance->getSystem(xr::SystemGetInfo{xr::FormFactor::HeadMountedDisplay}, d);

        xr::SessionCreateInfo createInfo{xr::SessionCreateFlagBits::None, systemId};
#if defined(XR_USE_GRAPHICS_API_OPENGL) && defined(XR_USE_PLATFORM_XLIB)
        xr::GraphicsBindingOpenGLXlibKHR glXlibBinding;
        if (backend == Backend::OpenGL) {
            auto requirements = instance->getOpenGLGraphicsRequirementsKHR(systemId, d);
            (void)requirements;
            createInfo.next = get(glXlibBinding);
        }
#endif // defined(XR_USE_GRAPHICS_API_OPENGL) && defined(XR_USE_PLATFORM_XLIB)
#ifdef XR_USE_GRAPHICS_API_VULKAN
        xr::GraphicsBindingVulkanKHR vulkanBinding;
        if (backend == Backend::Vulkan) {
            auto requirements = instance->getVulkanGraphicsRequirementsKHR(systemId, d);
            (void)requirements;
            createInfo.next = get(vulkanBinding);
        }
#endif // XR_USE_GRAPHICS_API_VULKAN
#ifdef XR_USE_GRAPHICS_API_D3D11
        xr::GraphicsBindingD3D11KHR D3D11Binding;
        if (backend == Backend::D3D11) {
            auto requirements = instance->getD3D11GraphicsRequirementsKHR(systemId, d);
            createInfo.next = get(D3D11Binding);
        }
#endif // XR_USE_GRAPHICS_API_D3D11

        session = instance->createSessionUnique(createInfo, d);
    }

    pollEvent(instance.get(), d);

    xr::UniqueDynamicSpace local;
    // Populate spaces
    {
        local = session->createReferenceSpaceUnique(
            xr::ReferenceSpaceCreateInfo{xr::ReferenceSpaceType::Local, xr::Posef{}}, d);
    }
    auto viewConfigType = xr::ViewConfigurationType::PrimaryStereo;

    // Begin session
    session->beginSession({viewConfigType}, d);

    pollEvent(instance.get(), d);

    // "Frame" loop
    // for (int frameNum = 0; frameNum < 20; ++frameNum) {
    int frameNum = 0;
    while (1) {
        ++frameNum;
        pollEvent(instance.get(), d);
        xr::Time t;

        if (backend == Backend::Headless) {
            t = getXrTimeNow(instance.get(), d);
        } else {
            // begin frame
            xr::Result ret = session->beginFrame(xr::FrameBeginInfo{}, d);
            if (ret == xr::Result::SessionLossPending) {
                // Can't really do too much.
                return 1;
            }

            // wait frame - gives us our time
            auto frameState = session->waitFrame(xr::FrameWaitInfo{});
            t = frameState.predictedDisplayTime;
        }

        // No wait frame because headless.

        {
            std::cout << "Iteration: " << frameNum << "\n";
            // Locate views and output them.
            xr::ViewState viewState{};
            auto views = session->locateViewsToVector({viewConfigType, t, local.get()}, put(viewState), d);

            for (uint32_t i = 0; i < numViews_; ++i) {
                std::cout << "View index " << i << ": ";
                print(views[i].pose);
                std::cout << "\n";
            }
            // Must manually sleep since we aren't waiting on a frame.
            std::this_thread::sleep_for(std::chrono::milliseconds(250));
        }

        if (backend != Backend::Headless) {
            // end frame
            session->endFrame(xr::FrameEndInfo{t, xr::EnvironmentBlendMode::Opaque, 0, nullptr}, d);
        }
    }
    session->requestExitSession(d);
    session->endSession(d);

    return 0;
}
