# OpenXR Tracker Printer

Utility for viewing tracker (pose, position, orientation) data from runtimes implementing the [Khronos OpenXR API][openxr].

Maintained as a part of the [Monado project](http://monado.freedesktop.org).

[openxr]: https://khronos.org/openxr

## Quick Start

[CMake][] 3.9 or newer is a required build-time dependency.

You will also need a C++14-capable compiler
at least for now.

To run, you'll need an OpenXR runtime implementing the `XR_MND_headless`
extension (operation without it is experimental), and
`XR_KHR_convert_timespec_time` (or
`XR_KHR_win32_convert_performance_counter_time` on Windows) extensions.

Build like a typical CMake project.

## Primary Authors and Contributors

- **Ryan Pavlik** - *Initial work* - @rpavlik on many platforms, ryan.pavlik on the others

## License

All files created for this project are licensed under the
Boost Software License v1.0 (BSL-1.0).
A handful of script/tooling files are reused from other projects,
and thus carry the license they were originally released with
(all are permissive free software licenses).
Most files in `vendor/` are from third-party projects and are subject to their respective licenses,
as is the code in any dependencies.

All files in this project, except for those copied directly from other projects and placed in `vendor/`,
shall have `SPDX-License-Identifier:` tags as well as a copyright statement.

## Acknowledgments

- [Collabora](https://collabora.com) for supporting Ryan's development and maintenance of this code
  in the course of his work.

[CMake]: https://cmake.org

## Contributing, Code of Conduct

See `CONTRIBUTING.md` for details of contribution guidelines.

Please note that this project is released with a Contributor Code of Conduct.
By participating in this project you agree to abide by its terms.

We follow the standard freedesktop.org code of conduct,
available at <https://www.freedesktop.org/wiki/CodeOfConduct/>,
which is based on the [Contributor Covenant](https://www.contributor-covenant.org).

Instances of abusive, harassing, or otherwise unacceptable behavior may be
reported by contacting:

- First-line Monado project contacts:
  - Ryan Pavlik <ryan.pavlik@collabora.com>
  - Jakob Bornecrantz <jakob@collabora.com>
- freedesktop.org contacts: see most recent list at <https://www.freedesktop.org/wiki/CodeOfConduct/>

---

## Copyright and License for this README.md file

For this file only:

> Copyright 2018-2019 Collabora, Ltd.
> Code of Conduct section: excerpt adapted from the [Contributor Covenant](https://www.contributor-covenant.org), version 1.4.1,
> available at <https://www.contributor-covenant.org/version/1/4/code-of-conduct.html>,
> and from the freedesktop.org-specific version of that code,
> available at <https://www.freedesktop.org/wiki/CodeOfConduct/>
>>
> SPDX-License-Identifier: CC-BY-4.0
